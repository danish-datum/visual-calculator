"use strict";
//extent is using for prototype
var extend = function (o) {
	function F() {}
	F.prototype = o;
	return new F();
}
//main container of the application
var Container = function (){
	this.divWidth =  '251px';
	this.divHeight =  '302px';
	this.divId =  'main';
	this.divAppend = 'resp-calculator';
}
//It will be used to display divs, style divs and add textfield in html document
var DisplayDiv = {
	displayContainer: function(obj){
		var newdiv = document.createElement("div"); // Create a <div>
		newdiv.id = obj.divId;
		newdiv.style.width = obj.divWidth;
		newdiv.style.height = obj.divHeight;
		document.getElementById(obj.divAppend).appendChild(newdiv);
	}, 
	divFloafLeft: function(divId){
		document.getElementById(divId).style.float = 'left';
	},
	addtextfield: function(divId){
		var inputtext = document.createElement("input");
		inputtext.id = 'txt-screen';
		inputtext.type = "text";
		inputtext.maxlength = '10';
		inputtext.style.width = '190px';
		inputtext.style.height = '46px';
		inputtext.style.float = 'left';
		document.getElementById(divId).appendChild(inputtext); // put it into the DOM
		document.getElementById("txt-screen").maxLength = "40";
		document.getElementById('txt-screen').focus();
	}
}
//This chunk of code is for generating buttons
var generateButton = {
	createbutton: function(objCreateButton){
		var newbutton = document.createElement("BUTTON");
		newbutton.id = objCreateButton.buttonId;
		var t = document.createTextNode(objCreateButton.buttonvalue);
		newbutton.appendChild(t);                                // Append the text to <button>
		document.getElementById(objCreateButton.divAppend).appendChild(newbutton);
		this.buttonstyle(objCreateButton);
	},
	buttonstyle:function(objCreateButton){
		document.getElementById(objCreateButton.buttonId).style.width = objCreateButton.buttonWidth;
		document.getElementById(objCreateButton.buttonId).style.height = objCreateButton.buttonHeight;
		document.getElementById(objCreateButton.buttonId).style.float = 'left';
	}
}

//Starting calculator interface

//creating main container
var objCont = new Container();
DisplayDiv.displayContainer(objCont);

//creating screen div
var objscreen = extend(objCont);
objscreen.divHeight = '65px';
objscreen.divId =  'screen';
objscreen.divAppend = objCont.divId;
DisplayDiv.displayContainer(objscreen);

//createing inner screen left div
var objscreenleftdiv = extend(objCont);
objscreenleftdiv.divWidth = '200px';
objscreenleftdiv.divHeight = '60px';
objscreenleftdiv.divId =  'screenLeftDiv';
objscreenleftdiv.divAppend = 'screen';
DisplayDiv.displayContainer(objscreenleftdiv);
DisplayDiv.divFloafLeft(objscreenleftdiv.divId);
DisplayDiv.addtextfield(objscreenleftdiv.divId);


//creating inner screen right div
var objscreenrightdiv = extend(objCont);
objscreenrightdiv.divWidth = '45px';
objscreenrightdiv.divHeight = '60px';
objscreenrightdiv.divId =  'clear-button';
objscreenrightdiv.divAppend = 'screen';
DisplayDiv.displayContainer(objscreenrightdiv);
DisplayDiv.divFloafLeft(objscreenrightdiv.divId);

//creating div for buttons
var objnumbersdiv = extend(objCont);
objnumbersdiv.divWidth = '251px';
objnumbersdiv.divHeight = '220px';
objnumbersdiv.divId =  'numbers-buttons';
objnumbersdiv.divAppend = objCont.divId;
DisplayDiv.displayContainer(objnumbersdiv);

//creating numeric buttons
var objCreateButton = extend(objCont);
objCreateButton.buttonWidth = '50px';
objCreateButton.buttonHeight = '50px';
for(var i=1;i<=9;i++){
	objCreateButton.buttonId = 'button_'+i;
	objCreateButton.buttonvalue = i;
	objCreateButton.divAppend = objnumbersdiv.divId;
	generateButton.createbutton(objCreateButton);
}
//creating zero button
objCreateButton.buttonId = 'button_0';
objCreateButton.buttonvalue = 0;
generateButton.createbutton(objCreateButton);

//creating add button
objCreateButton.buttonId = 'button_add';
objCreateButton.buttonvalue = '+';
generateButton.createbutton(objCreateButton);

//creating subtract button
objCreateButton.buttonId = 'button_sub';
objCreateButton.buttonvalue = '-';
generateButton.createbutton(objCreateButton);

//creating product button
objCreateButton.buttonId = 'button_prod';
objCreateButton.buttonvalue = '*';
generateButton.createbutton(objCreateButton);

//creating division button
objCreateButton.buttonId = 'button_div';
objCreateButton.buttonvalue = '/';
generateButton.createbutton(objCreateButton);

//creating decimal button
objCreateButton.buttonId = 'button_dot';
objCreateButton.buttonvalue = '.';
generateButton.createbutton(objCreateButton);

//This is the clear button which will be placed at top
objCreateButton.buttonId = 'btnclear';
objCreateButton.buttonHeight = '56px';
objCreateButton.buttonvalue = 'C';
objCreateButton.divAppend = objscreenrightdiv.divId;
generateButton.createbutton(objCreateButton);

//creating memory plus button
objCreateButton.buttonId = 'button_mplus';
objCreateButton.buttonvalue = 'M+';
objCreateButton.divAppend = objnumbersdiv.divId;
generateButton.createbutton(objCreateButton);

//creating memory minus button
objCreateButton.buttonId = 'button_mminus';
objCreateButton.buttonvalue = 'M-';
generateButton.createbutton(objCreateButton);

//creating memory recall button
objCreateButton.buttonId = 'button_mrecall';
objCreateButton.buttonvalue = 'MR';
generateButton.createbutton(objCreateButton);

//creating memory clear button
objCreateButton.buttonId = 'button_mclear';
objCreateButton.buttonvalue = 'MC';
generateButton.createbutton(objCreateButton);

//creating result button
objCreateButton.buttonId = 'button_equal';
objCreateButton.buttonvalue = '=';
generateButton.createbutton(objCreateButton);

//End Calculator Interface
/***************************************************************************/
//Given below code relates to the functionality of calculator.

//variables will be used for memory functions
var memoryvariable = {
	calcmemory: 0,
	calcresult: 0
}

//Function will get string and trim last charcter from the expression if it is alphabet or any special character.
var getstring = function(){
	var str = document.getElementById('txt-screen').value;
	var lastchr = str.substr(str.length -1 );
	var chcode = lastchr.charCodeAt();
	if((chcode < 42) || (chcode > 57)  ){
		str = str.replaceAt(str.length -1, '');
		document.getElementById('txt-screen').value = str;
	}
}
//clearscreen will be used to clear screen, it will be invoked by clear buttom or escape button on keyboard.
var clearscreen = function(){
	document.getElementById('txt-screen').value = '';
	memoryvariable.calcmemory = 0;
	memoryvariable.calcresult = 0;
	document.getElementById('txt-screen').focus();
}
//makecalculation is the main calculation area, it will be used for evaluating expessions.
var makecalculation = function(){
	var val = document.getElementById('txt-screen').value;
	if(val != ''){
		//if the = find in expression then it will break it.
		var splitedStr = val.split("=");
		var res = '';
		//if expression finds = sign then it will be executed here.
		if(splitedStr.length == 2){
			res = eval(splitedStr[0]);
			memoryvariable.calcresult = res;
			document.getElementById('txt-screen').value = splitedStr[0] + '=' + res;
		}else{
			//evaluation of expression will be done here.
			try {
			    res = eval(val);
			    memoryvariable.calcresult = res;
			}
			catch(e){
			    alert('An error has occurred: '+e.message)
			}
			finally{
			    document.getElementById('txt-screen').value = document.getElementById('txt-screen').value + '=' + res;
			}
		}
	}
}
///handleoperators will be used to remove operators, zeros from the start of the expression.
var handleoperators = function(){
	var str = document.getElementById('txt-screen').value;
	var flag = true;
	var allowedchars = ["0","+","-","*","/","."];
	for(var j=0;j<allowedchars.length;j++){
		var numofzeros = 0;
		if(str[0] == allowedchars[j]){
			for(var i=0;i<str.length;i++){
				if(flag){
					if(str[i] == allowedchars[j]){
						numofzeros++;
					}else{
						flag = false;
						break;
					}
				}
			}
			var limit = 0;
			if(allowedchars[j] == '.'){
				limit = 1;
			}
			if(numofzeros > limit){
				for(var i=0;i<=numofzeros;i++){
					str = str.replaceAt(i , '');
				}
			}
		}
	}
	document.getElementById('txt-screen').value = str;
}

//This is the event listner for click events, it will add operators, dots, and numbers.
document.getElementById(objnumbersdiv.divId).addEventListener( 'click', function ( event ) {
    var elementId = event.target.id;
    switch(elementId){
    	case 'button_add' :
    	case 'button_sub' :
    	case 'button_prod':
    	case 'button_div' :
    		document.getElementById('txt-screen').value = document.getElementById('txt-screen').value + event.target.innerHTML;
    		handleoperators();
    	break;
    	case 'button_dot' :
    		document.getElementById('txt-screen').value = document.getElementById('txt-screen').value + '.';
    		handleoperators();
    	break;
    	case 'button_equal' :
    		//this case will evaluate the whole expression.
    		makecalculation();
    	break;
    	case 'button_0' :
    	case 'button_1' :
    	case 'button_2' :
    	case 'button_3' :
    	case 'button_4' :
    	case 'button_5' :
    	case 'button_6' :
    	case 'button_7' :
    	case 'button_8' :
    	case 'button_9' :
    		var i = elementId.substring(7,8);
    		document.getElementById('txt-screen').value = document.getElementById('txt-screen').value + i;
    	break;
    }
});

//clear screen by clicking on clear button.
document.getElementById("btnclear").addEventListener( 'click', function () {
	clearscreen();
});

//on enter key returns result
document.getElementById("txt-screen").addEventListener( 'keydown', function (e) {
	//console.log(e.keyCode);
	var pressedKeyCode = e.keyCode;
	switch(pressedKeyCode){
		case 27:
			//on escape button it will clear screen.
			clearscreen();
		break;
		case 13:
			//bu pressing enter key, it will start calculating expression.
			makecalculation();
		break;
		default:
			var str = document.getElementById('txt-screen').value;
	    	var indexesArr = new Array();
	    	for(var i=0;i<str.length;i++){
				var chcode = str.charAt(i).charCodeAt();
				if((chcode < 42) || (chcode > 57)){
					indexesArr.push(i);
				}
			}
			var equalsignposition = 0;
			for(var i=0;i<indexesArr.length;i++){
				if(str[indexesArr[i]] == '='){
					equalsignposition = indexesArr[i];
				}else{
					str = str.replaceAt(indexesArr[i] , '');
				}
			}
			if(equalsignposition != 0){
				str = str.replaceequal(equalsignposition);
			}
	    	document.getElementById('txt-screen').value = str;
	    	handleoperators();
		break;
	}
});

String.prototype.replaceAt = function (index, char) {
    return this.substr(0, index) + char + this.substr(index + 1);
}

String.prototype.replaceequal = function(index){
	return this.substr(0, index);
}

document.getElementById("txt-screen").addEventListener( 'keyup', function (e) {
	var pressedKeyCode = e.keyCode;
	switch(pressedKeyCode){
		case 27:
			clearscreen();
		break;
		default:
			getstring();
			handleoperators();
		break;
	}
});

//adding memory on M+
document.getElementById("button_mplus").addEventListener( 'click', function () {
	memoryvariable.calcmemory = eval( memoryvariable.calcresult + memoryvariable.calcmemory );
});

//subtracting memory on M-
document.getElementById("button_mminus").addEventListener( 'click', function () {
	memoryvariable.calcmemory = eval( memoryvariable.calcmemory - memoryvariable.calcresult  );
}); 

//Recalling memory on MR
document.getElementById("button_mrecall").addEventListener( 'click', function () {
	document.getElementById('txt-screen').value = memoryvariable.calcmemory ;
});

//Clear memory on MC
document.getElementById("button_mclear").addEventListener( 'click', function () {
	memoryvariable.calcmemory = 0;
	document.getElementById('txt-screen').value = 0;
});